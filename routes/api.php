<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\ChatController;
use App\Http\Controllers\ChatsController;
use App\Http\Controllers\MessagesController;
use App\Http\Controllers\MiPerfilController;
use App\Http\Controllers\MisComprasController;
use App\Http\Controllers\MisPedidosController;
use App\Http\Controllers\NombreController;
use App\Http\Controllers\PedidosController;
use App\Http\Controllers\ProductoController;
use App\Http\Controllers\PublicacionController;
use App\Http\Controllers\PublicacionesVistasController;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::apiResource('testNombres', NombreController::class);




Route::post('register', [AuthController::class, 'register']);

Route::post('login', [AuthController::class, 'login']);



Route::post('subiradjunto', [PublicacionController::class, 'store']);
Route::post('cambiarEstado', [PublicacionController::class, 'cambiarEstado']);
Route::apiResource('publicacion', PublicacionController::class);
Route::get('productos', [ProductoController::class, 'index']);
Route::get('logout', [AuthController::class, 'logout']);

Route::apiResource('chat', ChatController::class);

/* Route::get('/user/chat/{nick_name}', [SearchController::class, 'usersIsFollow'])->name('usersIsFollow'); */

Route::apiResource('users', UserController::class);

Route::get('/user/chat/{chat_id}', [SearchController::class, 'messages'])->name('messages');

Route::apiResource('messages', MessagesController::class);

Route::apiResource('pedidos', PedidosController::class);

Route::apiResource('mispedidos', MisPedidosController::class);

Route::apiResource('miscompras', MisComprasController::class);

/* Route::put('/mispedidos/updateEstado/{id}', [MisPedidosController::class, 'updateEstado']); */

Route::apiResource('miperfil', MiPerfilController::class);

Route::apiResource('pedido', PedidosController::class);

Route::apiResource('chats', ChatsController::class);

Route::get('/user/publicacion/{publicacion_id}', [PedidosController::class, 'getPublicacion'])->name('getPublicacion');

Route::apiResource('publicacionesvistas', PublicacionesVistasController::class);
