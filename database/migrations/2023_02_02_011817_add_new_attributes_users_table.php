<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('sitioWeb')->nullable();
            $table->text('presentacion')->nullable();
            $table->boolean('status')->default(0);
            $table->string('apodo')->unique();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('sitioWeb');
            $table->dropColumn('presentacion');
            $table->dropColumn('status');
            $table->dropColumn('apodo');
        });
    }
};
