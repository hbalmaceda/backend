<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('productos')->insert([
            'name' => 'Iphone 13',
            'description' => 'Mobile Phone Apple',
            'amount' => 980,
        ]);
        DB::table('productos')->insert([
            'name' => 'Ipad Pro 11',
            'description' => 'Tablet Apple',
            'amount' => 850,
        ]);
        DB::table('productos')->insert([
            'name' => 'PlayStation 5',
            'description' => 'Videoconsole',
            'amount' => 540,
        ]);
    }
}
