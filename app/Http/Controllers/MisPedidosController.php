<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\CambiarEstadoPedidosRequest;
use App\Http\Requests\PedidosRequest;
use App\Http\Resources\PedidosResource;
use App\Models\Pedido;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MisPedidosController extends Controller
{
    public function index() {
        return PedidosResource::collection(Pedido::sort()->with(['publicacion'])->filter()->get());
    }

    public function update(CambiarEstadoPedidosRequest $request, Pedido $pedido) {

        /* DB::transaction(function () use ($request, $pedido) {
            $pedido->update($request->validated());

            return new PedidosResource($pedido);
        }); */

        $pedido = Pedido::find($request->id);

        $pedido->estado = $request->estado;

        $pedido->save();

        return new PedidosResource($pedido);


    }

    public function destroy(Pedido $pedido)
    {
        DB::transaction(function () use ($pedido) {
            $pedido->delete();
            return response()->noContent();
        });
    }
}
