<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Messages;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SearchController extends Controller
{
    /* public function usersIsFollow($nick_name)
    {
        return User::select('id', 'name', 'nick_name')->whereHas('followers', function (Builder $query) {
            $query->where('follower_id', auth()->user()->id);
        })->where('nick_name', 'like', '%' . $nick_name . '%');
    } */

    public function messages($chat_id) {
        return Messages::select('id', 'chat_id', 'user_id', 'message')->where('chat_id', $chat_id)->get();
    }
}
