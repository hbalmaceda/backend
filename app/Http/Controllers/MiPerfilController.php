<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;

class MiPerfilController extends Controller
{
    public function index()
    {
        return UserResource::collection(User::sort()->with(['post'])->filter()->get());
    }
}
