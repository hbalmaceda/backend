<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\PedidosRequest;
use App\Http\Resources\PedidosResource;
use App\Models\Pedido;
use App\Models\Publicacion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PedidosController extends Controller
{

    public function index()
    {
        return PedidosResource::collection(Pedido::sort()->with(['publicacion', 'user_publicacion', 'user_pedido'])->where('pedidos.estado', 'Pendiente')->orWhere('pedidos.estado', 'Aceptado')->filter()->get());
    }
    /* public function obtener()
    {
        return PedidosResource::collection(Pedido::sort()->with(['user_de_publicacion'])->filter()->get());
    } */

    public function getPublicacion($publicacion_id)
    {
        return Publicacion::select('id', 'titulo', 'descripcion', 'precio', 'path_adjunto', 'user_id')->where('id', $publicacion_id)->get();
    }

    public function obtenerPedidos($user_publicacion)
    {
        return PedidosResource::collection(Pedido::sort()->with(['user_publicacion'])->join('publicacion', 'publicacion.id', '=', 'pedidos.user_publicacion')->get());
    }


    public function store(PedidosRequest $request)
    {
        $pedido = DB::transaction(function () use ($request) {
            $pedido = Pedido::create($request->validated());

            return new PedidosResource($pedido);
        });
    }

    public function destroy(Pedido $pedido)
    {
        DB::transaction(function () use ($pedido) {
            $pedido->delete();
            return response()->noContent();
        });
    }
}
