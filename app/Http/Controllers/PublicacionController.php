<?php

namespace App\Http\Controllers;

use App\Http\Requests\CambiarEstadoRequest;
use App\Http\Requests\PublicacionRequest;
use App\Http\Resources\PublicacionResource;
use App\Models\Estado;
use App\Models\Publicacion;
use App\Utils\GuardarImagen;
use Illuminate\Support\Facades\DB;

class PublicacionController extends Controller
{

    public function index()
    {
        return PublicacionResource::collection(Publicacion::orderBy('id', 'desc')->sort()->filter()->get());
    }

    public function store(PublicacionRequest $request)
    {
        DB::transaction(function () use ($request) {
            $publicacion = new Publicacion();

            if ($request->fileAdjunto) {
                $guardar_img = new GuardarImagen();
                $respuesta = $guardar_img->guardar_imagen($request->fileAdjunto);
                list($path) = $respuesta;


                $publicacion->user_id = $request->user_id;
                $publicacion->titulo = $request->titulo;
                $publicacion->descripcion = $request->descripcion;
                $publicacion->precio = $request->precio;
                $publicacion->path_adjunto = $path;
                $publicacion->lat = $request->lat;
                $publicacion->long = $request->long;
                $publicacion->nombre_ubicacion = $request->nombre_ubicacion;
                $publicacion->save();
            }
        });
    }

    public function cambiarEstado(CambiarEstadoRequest $cambiarEstadoRequest)
    {

        /* dd($cambiarEstadoRequest); */
        DB::transaction(function () use ($cambiarEstadoRequest) {
            $estado = new Estado();

            $estado->estado = $cambiarEstadoRequest->estado;
            $estado->save();
        });
    }

    public function show(Publicacion $publicacion)
    {
        return new PublicacionResource($publicacion);
    }

     public function destroy(Publicacion $publicacion)
    {
        DB::transaction(function () use ($publicacion) {
            $publicacion->delete();
            return response()->noContent();
        });
    }
}
