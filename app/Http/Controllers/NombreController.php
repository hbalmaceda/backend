<?php

namespace App\Http\Controllers;

use App\Http\Resources\NombreResource;
use App\Models\Nombre;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class NombreController extends Controller
{
    public function index()
    {
        return NombreResource::collection(Nombre::all());
    }

    public function store(Request $request)
    {
        /*  DB::transaction(function () use ($request) {
        $nombre = Nombre::create($request);
        return new NombreResource($nombre);
        }); */
        $data = $request->all();

        return Nombre::create([
            'nombres' => $data['nombre'],
            'apellidos' => $data['apellido']
        ]);
    }
}
