<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\ChatRequest;
use App\Http\Resources\ChatResource;
use App\Models\Chat;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ChatController extends Controller
{
    public function index()
    {
        return ChatResource::collection(Chat::sort()->with(['usersent', 'userrecive', 'messages'])->get());
    }

    public function store(ChatRequest $request) {
        $chat = DB::transaction(function () use ($request) {
            $chat = Chat::create($request->validated());

            return new ChatResource($chat);
        });
    }
}
