<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\MessagesRequest;
use App\Http\Resources\MessagesResource;
use App\Models\Messages;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MessagesController extends Controller
{
    public function store(MessagesRequest $request)
    {
        $message = DB::transaction(function () use ($request) {
            $message = Messages::create($request->validated());

            return new MessagesResource($message);
        });
    }
}
