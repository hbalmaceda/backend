<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Resources\ChatResource;
use App\Models\Chat;
use Illuminate\Http\Request;

class ChatsController extends Controller
{
    public function index()
    {
        return ChatResource::collection(Chat::sort()->with(['messages'])->filter()->get());
    }
}
