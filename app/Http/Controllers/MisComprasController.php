<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Resources\PedidosResource;
use App\Models\Pedido;
use Illuminate\Http\Request;

class MisComprasController extends Controller
{
    public function index()
    {
        return PedidosResource::collection(Pedido::sort()->with(['publicacion', 'user_publicacion', 'user_pedido'])->where('pedidos.estado', 'Pagado')->filter()->get());
    }
}
