<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\PublicacionesVistasRequest;
use App\Http\Resources\PublicacionesVistasResource;
use App\Models\PublicacionesVistas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PublicacionesVistasController extends Controller
{
    public function index() {
        return PublicacionesVistasResource::collection(PublicacionesVistas::sort()->with(['user', 'publicacion'])->filter()->get());
    }

    public function store(PublicacionesVistasRequest $publicacionesVistasRequest) {
        $publicacionesVistas = DB::transaction(function () use ($publicacionesVistasRequest) {
            $publicacionesVistas = PublicacionesVistas::create($publicacionesVistasRequest->validated());

            return new PublicacionesVistasResource($publicacionesVistas);
        });
    }
}
