<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PublicacionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'fileAdjunto' => 'required|image',
            'titulo' => 'required',
            'precio' => 'required',
            'lat' => 'required',
            'long' => 'required',
            'nombre_ubicacion' => 'required',
        ];
    }
}
