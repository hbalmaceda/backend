<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PedidosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $rules = [
            'user_id' => 'required',
            'user_publicacion' => 'required',
            'publicacion_id' => 'required',
            'estado' => 'required',
        ];

        $pedido = $this->route()->parameter('mispedidos');

        if ($pedido) {
            $rules = [
                'user_id' => 'nullable',
                'user_publicacion' => 'nullable',
                'publicacion_id' => 'nullable',
                'estado' => 'required',
            ];
        }

        return $rules;
    }
}
