<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PublicacionesVistas extends Model
{
    use HasFactory;

    protected $table = 'publicaciones_vistas';

    protected $primaryKey = 'id';

    protected $fillable = [
        'user_id',
        'publicacion_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function publicacion()
    {
        return $this->belongsTo(Publicacion::class, 'publicacion_id');
    }

    public function scopeFilter(Builder $query)
    {
        if (!empty(request('user_id'))) {
            $user_id = request('user_id');
            $query->where('user_id', $user_id);
        }

        if (!empty(request('publicacion_id'))) {
            $publicacion_id = request('publicacion_id');
            $query->where('publicacion_id', $publicacion_id);
        }
    }

    public function scopeSort(Builder $query)
    {
        $campo = request('campo');
        $direccion = request('direccion');

        return $query->orderBy($campo, $direccion);
    }
}
