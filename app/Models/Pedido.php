<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pedido extends Model
{
    use HasFactory;

    protected $table = 'pedidos';

    protected $primaryKey = 'id';

    protected $fillable = [
        'user_id',
        'user_publicacion',
        'publicacion_id',
        'estado',
    ];

    public function user_pedido() {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function user_publicacion () {
        return $this->belongsTo(User::class, 'user_publicacion');
    }

    public function publicacion () {
        return $this->belongsTo(Publicacion::class);
    }

    public function scopeFilter(Builder $query)
    {
        if (!empty(request('user_publicacion'))) {
            $user_publicacion = request('user_publicacion');
            $query->where('user_publicacion', $user_publicacion);
        }

        if(!empty(request('user_id'))) {
            $user_id = request('user_id');
            $query->where('user_id', $user_id);
        }

        if(!empty(request('id_pedido'))) {
            $id_pedido = request('id_pedido');
            $query->where('id', $id_pedido);
        }
    }

    public function scopeSort(Builder $query)
  {
    $campo = request('campo');
    $direccion = request('direccion');

    return $query->orderBy($campo, $direccion);
  }
}
