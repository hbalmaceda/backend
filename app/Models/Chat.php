<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    use HasFactory;

    protected $table = 'chats';

    protected $primaryKey = 'id';

    protected $fillable = [
        'user_sent',
        'user_recive',
    ];

    public function usersent() {
        return $this->belongsTo(User::class, 'user_sent');
    }

    public function userrecive() {
        return $this->belongsTo(User::class, 'user_recive');
    }

    public function messages() {
        return $this->hasMany(Messages::class, 'chat_id');
    }

    public function scopeFilter(Builder $query)
    {
        if(!empty(request('user_sent'))) {
            $user_sent = request('user_sent');
            $query->where('user_sent', $user_sent)->orWhere('user_recive', $user_sent);
        }
    }

    public function scopeSort(Builder $query)
  {
    $campo = request('campo');
    $direccion = request('direccion');

    return $query->orderBy($campo, $direccion);
  }
}
