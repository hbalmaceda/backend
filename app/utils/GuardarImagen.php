<?php

namespace App\Utils;

use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class GuardarImagen
{
    function guardar_imagen($file)
    {
        //El ancho, alto, tipo y atributos
        list($width, $height, $type, $attr) = getimagesize($file);

        //Se ignora el nombre del archivo ya que algunos nombres
        //no tienen caracteres especiales que pueden crear problemas
        //se usa JPG ya que este formato pesa mucho menos que otros
        $file_name = time() . '_' . Str::random(10) . '.JPG';

        //La ruta donde se guarda
        $file_path = 'attachment/' . $file_name;

        //Ruta local de la imagen a subir
        $route = storage_path() . '\app\public\attachment/' . $file_name;

        //Peso del archivo
        $sizeFile = $file->getSize(); //resultado en byte

        //Si el peso es menor o igual a 200bk se guardar directamente
        //por que intervention en esos casos sube el peso del archivo
        if ($sizeFile <= 200000) { //mayor a 200kb
            $file->storeAs('attachment', $file_name, 'public');
        } else {
            //se verifica el el ancho o el alto es mayor para
            //reducir la parte mas grande y optimizar el peso.
            if ($width >= $height) {
                //Guardar con intervention
                Image::make($file)
                    //width, height
                    ->resize(900, null, function ($constraint) {
                        //aspectRatio() Mantiene proporcional la altura.
                        $constraint->aspectRatio();
                        //upsize() evitar agrandar la imagen en casa que sea mas pequeño que los parametros.
                        $constraint->upsize();
                    })
                    ->save($route);
            } else {
                //Guardar con intervention
                Image::make($file)
                    //width, height
                    ->resize(null, 900, function ($constraint) {
                        //aspectRatio() Mantiene proporcional la altura.
                        $constraint->aspectRatio();
                        //upsize() evitar agrandar la imagen en casa que sea mas pequeño que los parametros.
                        $constraint->upsize();
                    })
                    ->save($route);
            }
        }

        return [$file_name, $file_path];
    }
}
